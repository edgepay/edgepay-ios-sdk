Pod::Spec.new do |spec|

spec.platform     = :ios
spec.ios.deployment_target = "9.0"
spec.name         = "EdgePaySDK"
spec.summary      = "EdgePay iOS SDK allows merchants to easily and securely process payments from your iOS app."

spec.version      = "0.7.3"

spec.license      = { :type => "MIT", :file => "LICENSE" }

spec.author       = { "" => "" }

spec.homepage     = "https://bitbucket.org/edgepay/edgepay-ios-source.git"

spec.source       = { :git => "https://bitbucket.org/edgepay/edgepay-ios-source.git", :tag => "#{spec.version}" }

spec.source_files  = "EdgePaySDK/EdgePaySDK/**/*.swift"

spec.public_header_files = "EdgePaySDK/EdgePaySDK/*.h"

spec.swift_version = "4.2"

# spec.requires_arc = true
end